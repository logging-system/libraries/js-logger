import { request as httpRequest } from 'http';
import { request as httpsRequest } from 'https';
import { randomUUID } from 'crypto';
import { LogLevel, LogStruct } from '../contracts/LogTypes';
import { Logger } from '../contracts/Logger';

export class HTTPClientLogger implements Logger {
  constructor(private serviceName: string, private entryPointURL: string, private key: string) {}

  async trace(message: string, data: Record<string, any>): Promise<void> {
    this.log(this.prepareLogStruct(LogLevel.trace, message, data));
  }

  async debug(message: string, data: Record<string, any>): Promise<void> {
    this.log(this.prepareLogStruct(LogLevel.debug, message, data));
  }

  async info(message: string, data: Record<string, any>): Promise<void> {
    this.log(this.prepareLogStruct(LogLevel.info, message, data));
  }

  async warn(message: string, data: Record<string, any>): Promise<void> {
    this.log(this.prepareLogStruct(LogLevel.warn, message, data));
  }

  async error(message: string, data: Record<string, any>): Promise<void> {
    this.log(this.prepareLogStruct(LogLevel.error, message, data));
  }

  async fatal(message: string, data: Record<string, any>): Promise<void> {
    this.log(this.prepareLogStruct(LogLevel.fatal, message, data));
  }

  async log(log: LogStruct): Promise<void> {
    try {
      await this.request(log);
    } catch (e) {
      console.error(e);
    }
  }

  private async request(log: LogStruct): Promise<void> {
    return new Promise((resolve, reject) => {
      const options = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: this.key,
        },
      };

      const requestFunction = this.entryPointURL.startsWith('https') ? httpsRequest : httpRequest;

      const request = requestFunction(this.entryPointURL, options, (response) => {
        if (response.statusCode !== 201) {
          reject(response.statusCode);
        }

        resolve();
      });
      const json = JSON.stringify({ id: randomUUID(), ...log });
      request.write(json);
      request.end();
    });
  }

  private prepareLogStruct(logLevel: LogLevel, message: string, data: Record<string, any>): LogStruct {
    const currentTime = new Date();
    return {
      level: logLevel,
      message: message,
      service: this.serviceName,
      timestamp: currentTime.toISOString(),
      data: data,
    };
  }
}
