import { randomUUID } from 'crypto';
import { LogLevel, LogStruct } from '../contracts/LogTypes';
import { Logger } from '../contracts/Logger';

export class ServerLogger implements Logger {
  constructor(private serviceName: string = '') {}

  trace(message: string, data: Record<string, any>): void {
    this.log(this.prepareLogStruct(LogLevel.trace, message, data));
  }

  debug(message: string, data: Record<string, any>): void {
    this.log(this.prepareLogStruct(LogLevel.debug, message, data));
  }

  info(message: string, data: Record<string, any>): void {
    this.log(this.prepareLogStruct(LogLevel.info, message, data));
  }

  warn(message: string, data: Record<string, any>): void {
    this.log(this.prepareLogStruct(LogLevel.warn, message, data));
  }

  error(message: string, data: Record<string, any>): void {
    this.log(this.prepareLogStruct(LogLevel.error, message, data));
  }

  fatal(message: string, data: Record<string, any>): void {
    this.log(this.prepareLogStruct(LogLevel.fatal, message, data));
  }

  log(log: LogStruct): void {
    try {
      const json = JSON.stringify({ id: randomUUID(), ...log });
      console.log(json);
    } catch (e) {
      const currentTime = new Date();
      console.error(
        JSON.stringify({
          id: randomUUID(),
          level: LogLevel.fatal,
          message: 'Cannot encoding log to json',
          service: 'internal-server-log-library',
          timestamp: currentTime.toISOString(),
          data: {
            error: e,
          },
        })
      );
    }
  }

  private prepareLogStruct(logLevel: LogLevel, message: string, data: Record<string, any>): LogStruct {
    const currentTime = new Date();
    return {
      level: logLevel,
      message: message,
      service: this.serviceName,
      timestamp: currentTime.toISOString(),
      data: data,
    };
  }
}
