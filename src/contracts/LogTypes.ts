export enum LogLevel {
  trace = 1,
  debug = 2,
  info = 3,
  warn = 4,
  error = 5,
  fatal = 6,
}

export type LogStruct = {
  level: LogLevel;
  message: string;
  service: string;
  timestamp: string;
  data?: Record<string, any>;
};
