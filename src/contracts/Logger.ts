import { LogLevel, LogStruct } from './LogTypes';

export interface Logger {
  trace: (message: string, data: Record<string, any>) => void;
  debug: (message: string, data: Record<string, any>) => void;
  info: (message: string, data: Record<string, any>) => void;
  warn: (message: string, data: Record<string, any>) => void;
  error: (message: string, data: Record<string, any>) => void;
  fatal: (message: string, data: Record<string, any>) => void;
  log: (log: LogStruct) => void;
}
