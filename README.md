# Dastanaron/log-system-logger

This is a package for logging your applications with the [dastanaron/logging-system](https://gitlab.com/logging-system). Server logs are created for logging in json format, which is well suited for grabbers of this system

## How it use

```typescript
import { ServerLogger } from '@dastanaron/log-system-logger';

const logger = new ServerLogger('my-service-logger');

logger.info('info message', { foo: 'bar', bar: 'buz' });

try {
  someMethod();
} catch (e) {
  logger.error('Error of someMethod', {
    error: e.message,
    stack: e.stack,
  });
}
```
