import { describe, expect, test } from '@jest/globals';
import { ServerLogger } from '../src/serverLogger/ServerLogger';

describe('Server logger', () => {
  const logger = new ServerLogger('test-logger');

  test('trace', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation();
    logger.trace('test message', { foo: 'bar' });

    expect(JSON.parse(spy.mock.calls[0][0])).toEqual(
      expect.objectContaining({
        level: 1,
        message: 'test message',
        service: 'test-logger',
        data: { foo: 'bar' },
      })
    );

    spy.mockRestore();
  });

  test('debug', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation();
    logger.debug('debug message', { type: 'debug' });

    expect(JSON.parse(spy.mock.calls[0][0])).toEqual(
      expect.objectContaining({
        level: 2,
        message: 'debug message',
        service: 'test-logger',
        data: { type: 'debug' },
      })
    );

    spy.mockRestore();
  });

  test('info', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation();
    logger.info('info message', { type: 'info' });

    expect(JSON.parse(spy.mock.calls[0][0])).toEqual(
      expect.objectContaining({
        level: 3,
        message: 'info message',
        service: 'test-logger',
        data: { type: 'info' },
      })
    );

    spy.mockRestore();
  });

  test('warn', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation();
    logger.warn('warn message', { type: 'warn' });

    expect(JSON.parse(spy.mock.calls[0][0])).toEqual(
      expect.objectContaining({
        level: 4,
        message: 'warn message',
        service: 'test-logger',
        data: { type: 'warn' },
      })
    );

    spy.mockRestore();
  });

  test('error', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation();
    logger.error('error message', { type: 'error' });

    expect(JSON.parse(spy.mock.calls[0][0])).toEqual(
      expect.objectContaining({
        level: 5,
        message: 'error message',
        service: 'test-logger',
        data: { type: 'error' },
      })
    );

    spy.mockRestore();
  });

  test('fatal', () => {
    const spy = jest.spyOn(console, 'log').mockImplementation();
    logger.fatal('fatal message', { type: 'fatal' });

    expect(JSON.parse(spy.mock.calls[0][0])).toEqual(
      expect.objectContaining({
        level: 6,
        message: 'fatal message',
        service: 'test-logger',
        data: { type: 'fatal' },
      })
    );

    spy.mockRestore();
  });
});
