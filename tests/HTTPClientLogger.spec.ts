import { describe, expect, test } from '@jest/globals';
import { HTTPClientLogger } from '../src/clientLogger/HTTPClientLogger';

describe('HTTP Client logger', () => {
  const logger = new HTTPClientLogger('test-logger', 'http://log-system.local/api/log/create', 'examplekey');

  test('trace', async () => {
    await logger.trace('test message', { foo: 'bar' });

    expect(true).toBe(true);
  }, 3000);
});
